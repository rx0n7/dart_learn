//01 - Passando parametro pela funcao com o tipo
void showMenssager(String name) {
  //03 - Usando "nome" abaixo
  print("Alerta, voce nao preencheu todos os campos $name !!!");
  print("Sua idade eh: ");
}

void main() {
  //02 - Passando o argumento pra funcao
  showMenssager("Renan");
  /*
    Alerta, voce nao preencheu todos os campos Renan !!!
    Sua idade eh: 
  */
}
