//

void showMenssager(String name, int age) {
  print("Alerta, voce nao preencheu todos os campos $name !!!");
  print("Sua idade eh: $age ");
}

//01 - Adcionando bonus como "double" pois o bonus poderia ser 500,0
void calcPay(double pay, double bonus) {
  var amount = pay - (pay * 0.1) + bonus; //03 - Somando o bonus
  print("Salario total: $amount ");
  //Salario total: 1400.0
}

void main() {
  var bonus = 500;
  //02 - Passando o bonus como variavel
  calcPay(1000, bonus);
  // Error - Passando um inteiro para uma funcao double
}
