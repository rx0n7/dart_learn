//01 - Definindo um novo parametro, que eh "age" do tipo inteiro
void showMenssager(String name, int age) {
  print("Alerta, voce nao preencheu todos os campos $name !!!");
  //02 - Usando a idade normalmente
  print("Sua idade eh: $age ");
}

void main() {
  //03 - Passando o valor da idade por parametro
  showMenssager("Renan", 30);
  /*
    Alerta, voce nao preencheu todos os campos Renan !!!
    Sua idade eh: 30  
  */
}
