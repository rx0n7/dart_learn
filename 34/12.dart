void showData(String name, int age, {double height}) {
  var newHeight = height ?? 0;

  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $newHeight");
}

void calcBonus() {
  print("seu bonus eh de 20");
}

void calcSalary(double salary, Function functionParameter) {
  print("Seu salario eh: $salary");
  //01 - Chamando a funcao pelo nome, que eh o mesmo que chamar a "calcBonus" ou qualquer outra
  functionParameter();
}

void main() {
  calcSalary(100, calcBonus);
  /*
    Seu salario eh: 100.0
    seu bonus eh de 20
  */
}
