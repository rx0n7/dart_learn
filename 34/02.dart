//01 - Deixando os parametro opcionais com "{}" as chaves
void showData(String name, {int age, double height}) {
  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $height");
}

void main() {
  //02 - Passando apenas o "nome" e os valores que nao foram passados aparecem com "null"
  showData("Renan");
  /* Nome: Renan
     Idade: null
     Altura: null 
  */
}
