void showData(String name, {int age, double height}) {
  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $height");
}

void main() {
  showData("Renan", age: 39, height: 19.7);
  /*
  Nome: Renan
Idade: 39
Altura: 19.7
 */
}
