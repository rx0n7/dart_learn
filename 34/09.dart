void showData(String name, int age, {double height}) {
  var newHeight = height ?? 0;

  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $newHeight");
}

void calcSalary(double salary) {
  print("Seu salario eh: $salary");
  //Seu salario eh: 100.0
}

void main() {
  calcSalary(100);
}
