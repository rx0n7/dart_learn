//01 - Passando somente um parametro como opcional
void showData(String name, int age, {double height}) {
  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $height");
}

void main() {
  //02 - Passando os outros parametros obrigatorios, e "altura" que eh opcional
  showData("Renan", 39, height: 19.7);
  /*
    Nome: Renan
    Idade: 39
    Altura: 19.7
 */
}
