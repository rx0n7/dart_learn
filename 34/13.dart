void showData(String name, int age, {double height}) {
  var newHeight = height ?? 0;

  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $newHeight");
}

void calcBonus() {
  print("seu bonus eh de 20");
}

void calcSalary(double salary, Function functionParameter) {
  print("Seu salario eh: $salary");
  functionParameter();
}

void main() {
  //01 - Criando uma funcao anonima
  calcSalary(100, () {
    //02 - Copiando o "print"
    print("seu bonus eh de 20");
    /* 
      Seu salario eh: 100.0
      seu bonus eh de 20
    */
  });
}
