void showData(String name, int age, {double height}) {
  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $height");
}

void main() {
  //01 - Retirando a altura e executa normalmente
  showData("Renan", 39);
  /*
  Nome: Renan
  Idade: 39
  Altura: null
 */
}
