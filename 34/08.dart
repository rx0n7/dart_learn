//Ps. Os parametros opcionais semfre ficaram por ultimo e nunca por primeiro
void showData(String name, int age, {double height}) {
  var newHeight = height ?? 0;

  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $newHeight");
}

void main() {
  showData("Renan", 39, height: 17.8);
  /* 01 - Colocando a altura ele fica com o valor da altura, se nao colocar ele fica com valor zero
    Nome: Renan
    Idade: 39
    Altura: 17.8
 */
}
