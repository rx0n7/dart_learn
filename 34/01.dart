//01 - Pasando os parametros para a funcao "showData" com nome do tipo "string", idade do tipo "int" e altura do tipo "double"
void showData(String name, int age, double height) {
  //02 - Usando o "print" pra exibir os dados
  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $height");
}

void main() {
  //03 - Chmando a funcao e pssando os parametros
  showData("Renan", 16, 1.73);
  /*
    Nome: Renan
    Idade: 16
    Altura: 1.73
  */
}
