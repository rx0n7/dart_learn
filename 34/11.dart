void showData(String name, int age, {double height}) {
  var newHeight = height ?? 0;

  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $newHeight");
}

//01 - Funcao apenas com um "print" e sem nenhum calculo
void calcBonus() {
  print("seu bonus eh de 20");
}

//02 - Passando uma funcao como parametro chamada "functionParameter"
void calcSalary(double salary, Function functionParameter) {
  print("Seu salario eh: $salary");
  //04 - Executando a funcao "functionParameter"
  calcBonus();
}

void main() {
  //03 - Passando como parametro a funcap "calcBonus" para a funcao "calcSalary"
  calcSalary(100, calcBonus);
  /*
    Seu salario eh: 100.0
    seu bonus eh de 20
  */
}
