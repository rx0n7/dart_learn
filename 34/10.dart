void showData(String name, int age, {double height}) {
  var newHeight = height ?? 0;

  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $newHeight");
}

//01 -  Funcao que imprime o valor do salario passado
void calcSalary(double salary) {
  print("Seu salario eh: $salary");
}

void main() {
  //02 - Chamando a funcao "calcSalary" passando o valor do salario
  calcSalary(100);
  //Seu salario eh: 100.0
}
