void showData(String name, {int age, double height}) {
  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $height");
}

void main() {
  //01 - Temos que definir qual parametro opcional estamos passando
  showData("Renan", age: 39);
  /*
    Nome: Renan
    Idade: 39
    Altura: null  
  */
}
