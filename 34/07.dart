void showData(String name, int age, {double height}) {
  //01 - Fzendo um teste criando uma variavel chmada nova altura e verificando se ela esta nula, e colocamos zero ao ives de nulo, 
  var newHeight = height ?? 0;

  print("Nome: $name");
  print("Idade: $age");
  print("Altura: $newHeight");
}

void main() {
  showData("Renan", 39);
  /* 02 - Agora ele nao imprime mais nulo na altura, pois o operador testor se a altura foi nula, se foi ele configura o valor padrao, caso contrario ele configura a propria altura 
    Nome: Renan
    Idade: 39
    Altura: 0.0
 */
}
