class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }
}

class Dog extends Animal {
  void latir() {
    print("latir");
  }
}

class Bird extends Animal {
  void voar() {
    print("voar");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.color = "Amarelo";
  print(dog.color);
  dog.latir();

  bird.color = "Marron";
  print(bird.color);
  bird.voar();
  /*
    Amarelo
    latir
    Marron
    voar
  */
}
