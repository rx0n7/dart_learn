class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }
}

class Dog extends Animal {
  //
}

class Bird extends Animal {
  //
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.color = "Amarelo";
  print(dog.color);

  bird.color = "Marron";
  print(bird.color);
  //Amarelo
  //Marron
}
