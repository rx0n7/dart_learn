class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }
}

class Dog extends Animal {
  String colorEar;
  void latir() {
    print("latir");
  }
}

class Bird extends Animal {
  String colorBeak;
  void voar() {
    print("voar");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.color = "Amarelo";
  dog.colorEar = "preto";

  print("Cor do cao: " + dog.color);
  print("Cor da orelha: " + dog.colorEar);

  dog.latir();

  bird.color = "Marron";
  print(bird.color);

  bird.colorBeak = "verde";
  print("Cor do bico do passaro: " + bird.colorBeak);

  bird.voar();
  /*
    Cor do cao: Amarelo
    Cor da orelha: preto
    latir
    Marron
    voar
  */
}
