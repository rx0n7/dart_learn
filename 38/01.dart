class Dog {
  String color;
  void sleep() {
    print("Dormir");
    //
  }
}

class Bird {
  String color;
  void sleep() {
    print("Dormir");
    //
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = Bird();

  bird.color = "Branco";
  print(bird.color);
  //Branco
}
