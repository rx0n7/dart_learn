class Dog {
  String color;
  void sleep() {
    print("Dormir");
    //
  }
}

class Bird {
  String color;
  void sleep() {
    print("Dormir");
    //
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.color = "Amarelo";
  print(dog.color);

  bird.color = "Marron";
  print(bird.color);
  //Amarelo
  //Marron
}
