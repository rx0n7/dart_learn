class Acount {
  double balance = 0;
  double _withdrawal = 0;

  double get withdrawal {
    return this._withdrawal;
  }

  //Setter -> Configurar
  set withdrawal(double withdrawal) {
    //01 -
    if (withdrawal > 0 && withdrawal <= 500) {
      //02
      this._withdrawal = withdrawal;
    }
  }
}

void main() {
  Acount acount = Acount();
  acount.withdrawal = 500;

  print(acount.withdrawal);
  //500.0
}
