class Acount {
  double balance = 0;
  double _withdrawal = 500;

  double get withdrawal {
    return this._withdrawal;
  }

  //01 - Setter Configura um valor
  set withdrawal(double withdrawal) {
    //02 - Configurando o saque que veio como parametro
    this._withdrawal = withdrawal;
  }
}

void main() {
  Acount acount = Acount();
  acount.withdrawal = 900;

  print(acount.withdrawal);
  //900.0
}
