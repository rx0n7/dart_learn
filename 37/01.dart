class Acount {
  //01 - Definindo os valores iniciais de saque e saldo
  double balance = 0;
  double withdrawal = 0;

  //Getter -> Obter
  //Setter -> Configurar

}

void main() {
  //02 - Instanciando a classe conta
  Acount acount = Acount();
  //03 - Configurando o saque que o usuario digitou
  acount.withdrawal = 400;

  //04 - Imprimindo o valor
  print(acount.withdrawal);
  //400.0
}
