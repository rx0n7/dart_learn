class House {
  String color;
  void openWindow() {
    print("Abrir janela da casa");
   
  }
}

void main() {

  House myHouse = new House();
  myHouse.color = "Amarela";
  // 01 - Acessando o metodo com o ponto "."
  myHouse.openWindow();

  print(myHouse.color);
  /*
    Abrir janela da casa 
    Amarela
 */
}
