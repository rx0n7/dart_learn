//Classe
class House {
  // Atributos
  String color;
  // Metodos
  void openWindow(int amountWindow) {
    print("Abrir janela da casa $color");
    print("Abrir janela da casa $amountWindow");
  }

  //01 - Criando um metodo sem paramentro
  void openPort() {
    print("Abrir porta com a cor $color");
  }
}

void main() {
  House myHouse = House();
  myHouse.color = "Amarela";
  myHouse.openWindow(5);

  //02 - Chmando o metodo abrir porta
  myHouse.openPort();
  /*
    brir janela da casa Amarela
    Abrir janela da casa 5
    Abrir porta com a cor Amarela
 */
}
