//Classe
class House {
  String color;

  void openWindow(int amountWindow) {
    //01 - Colocando o atributo "cor" aqui
    print("Abrir janela da casa $color");
    print("Janelas: $amountWindow");
  }
}

void main() {
  House myHouse = House();

  myHouse.color = "Amarela";
  myHouse.openWindow(5);
  /*
  Abrir janela da casa Amarela
  Janelas: 5
  */
}
