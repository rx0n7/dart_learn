//Classe
class House {
  String color;

  void openWindow(int amountWindow) {
    print("Abrir janela da casa $color");
    print("Janelas: $amountWindow");
    
  }
}

void main() {
  House myHouse = House();
  myHouse.color = "Amarela";
  myHouse.openWindow(5);

  //01 - Instanciando uma "casa2" e definindo uma nova cor e um novo numero de janelas
  House myHouse2 = House();
  myHouse2.color = "Vermelho";
  myHouse2.openWindow(3);
/*
    Abrir janela da casa Amarela
    Abrir janela da casa 5

    Abrir janela da casa Vermelho
    Abrir janela da casa 3
 */
}
