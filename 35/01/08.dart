class House {
  String color;


  //01 - Agora temos duas funcoesc, um abrir janela da casa "cor" e outro abrir porta da casa "cor"
  // void openWindow(int amountWindow) {
  void openWindow() {
    print("Abrir janela da casa $color");
 //   print("Abrir janela da casa $amountWindow");
  }

  void openPort() {
    print("Abrir porta com a cor $color");
  }
  
  void  openHouse() {
    //02 - Referenciando a classe com "this"
    this.openWindow();
    this.openPort();
  }

}

void main() {
  House myHouse = House();
  myHouse.color = "Amarela";
  //myHouse.openWindow();
  //myHouse.openPort();

  //03 - Chamando o abrir casa ele execulta os dois metodos
  myHouse.openHouse();
  //Abrir janela da casa Amarela
  //Abrir porta com a cor Amarela
}
