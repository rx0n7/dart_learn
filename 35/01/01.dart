//01 - Classe casa com atributos "color" 
class House {
  String color;
}

void main() {
//02 - Transformando a classe em objeto, definimos "name"
  String name = "Renan";
//03 - "Casa" com letra maiuscula, que recebe "new house, minha casa eh do tipo "casa", que eh um objeo que instancimamos
  House myHouse = new House();
  //03 - Acessando as cores com "." ponto e configuramos a cor na variavel
  myHouse.color = "Amarela";

  //04 - Imprimindo acessando a cor com "."
  print(myHouse.color);
  //Amarela
}
