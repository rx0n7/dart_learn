class House {
  String color;

  //01 - Passsamdp parametro para o metodo,  que eh a quantidade de janelas
  void openWindow(int amountWindow) {
    //02 - Exibindo a quantidade de janelas com o dolar "$"
    print("Abrir janela da casa $amountWindow");
  }
}

void main() {
  House myHouse = new House();
  myHouse.color = "Amarela";
  //03 - Passando a quantidade de janelas por paramentro
  myHouse.openWindow(5);

  print(myHouse.color);
  /*
    Abrir janela da casa 5
    Amarela
 */
}
