class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }

  void run() {
    print("Correr");
  }
}

class Dog extends Animal {
  String colorEar;
  void latir() {
    print("latir");
  }

  //01 - Fazendo uma sobreposicao com "overide"
  @override
  void run() {
    print("Correr como um cao");
  }
}

class Bird extends Animal {
  String colorBeak;
  void voar() {
    print("voar");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.run();
  bird.run();

  /*
    Correr como um cao
   Correr
   */
}
