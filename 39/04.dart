class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }

  void run() {
    print("Correr");
  }
}

class Dog extends Animal {
  String colorEar;
  void latir() {
    print("latir");
  }

  @override
  void run() {
    print("Correr como um cao");
  }
}

class Bird extends Animal {
  String colorBeak;
  void voar() {
    print("voar");
  }

  //01 - Sobrepondo passaro
  @override
  void run() {
    print("Correr como um passaro");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.run();
  bird.run();

  // Correr como um cao
  //Correr como um passaro
}
