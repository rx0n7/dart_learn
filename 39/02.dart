class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }

  //01 - Metodo correr que tah na classe pai
  void run() {
    print("Correr");
  }
}

class Dog extends Animal {
  String colorEar;
  void latir() {
    print("latir");
  }
}

class Bird extends Animal {
  String colorBeak;
  void voar() {
    print("voar");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  //02 - Usando o correr
  dog.run();
  bird.run();
  /*
    Correr
    Correr
  */
}
