class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }
}

class Dog extends Animal {
  String colorEar;
  void latir() {
    print("latir");
  }
}

class Bird extends Animal {
  String colorBeak;
  void voar() {
    print("voar");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.sleep();
  bird.sleep();
  /*
   Dormir
   Dormir
  k*/
}
