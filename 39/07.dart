class Animal {
  String color;

  //01 - Construtor que recebe uma cor
  Animal(this.color);

  void sleep() {
    print("Dormir");
  }

  void run() {
    print("Correr como um");

    print("Correr ");
    print("como um");
    print("um");
  }
}

class Dog extends Animal {
  String colorEar;

  //02 - Construtor recebe a co do cao e  a cor da orelha(que esta na classe)
  // e que configura a classe Animal passando a cor pra la com "super"
  Dog(String color, this.colorEar) : super(color);

  void latir() {
    print("latir");
  }

  @override
  void run() {
    super.run();
    print(" cao");
  }
}

class Bird extends Animal {
  String colorBeak;
  //03 - Confiugrando a cor do passaro a cor e a cor do bico
  Bird(String color, this.colorBeak) : super(color);

  void voar() {
    print("voar");
  }

  @override
  void run() {
    super.run();
    print(" passaro");
  }
}

void main() {
  //04 - Passando os dados para os construtores
  Dog dog = new Dog("Marrom", "Branco");
  Bird bird = new Bird("Verde", "Amarelo");
  //05 - Fazendo a concatenacao usando o "$"
  print("Cao cor: ${dog.color}, Cor do Bico: ${dog.colorEar}");
  print("Passaro cor: ${bird.color}, Cor do Bico: ${bird.colorBeak}");

  /*
    Cao cor: Marrom, Cor do Bico: Branco
    Passaro cor: Verde, Cor do Bico: Amarelo
 */
}
