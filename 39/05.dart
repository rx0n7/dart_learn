class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }

  //01 - Utilizando "Correr como um" na classe pai
  void run() {
    print("Correr como um");
  }
}

class Dog extends Animal {
  String colorEar;
  void latir() {
    print("latir");
  }

  @override
  void run() {
    //02 - Acessando o metodo da classe pai "Run" atraves do "super"
    super.run();
    print(" cao");
  }
}

class Bird extends Animal {
  String colorBeak;
  void voar() {
    print("voar");
  }

  @override
  //03 - Fazer o mesmo com passaro
  void run() {
    super.run();
    print(" passaro");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.run();
  bird.run();
  /*
    Correr como um
    cao
    Correr como um
    passaro
 */
}
