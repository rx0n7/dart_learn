class Animal {
  String color;
  void sleep() {
    print("Dormir");
  }

  void run() {
    print("Correr como um");
    //01 - Simulando varias linhas de codigo
    print("Correr ");
    print("como um");
    print("um");
  }
}

class Dog extends Animal {
  String colorEar;
  void latir() {
    print("latir");
  }

  @override
  void run() {
    super.run();
    print(" cao");
  }
}

class Bird extends Animal {
  String colorBeak;
  void voar() {
    print("voar");
  }

  @override
  void run() {
    super.run();
    print(" passaro");
  }
}

void main() {
  Dog dog = new Dog();
  Bird bird = new Bird();

  dog.run();
  bird.run();
  /*
    Correr como um
    Correr 
    como um
    um

    cao
    Correr como um
    Correr 
    como um
    um
    passaro
 */
}
