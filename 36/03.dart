class User {
  String user;
  String password;

  //01 - Passar dados por parametros como define parametros
  User(String user, String password) {
    //03 - Configurando os parametros nos atributos
    this.user = user;
    this.password = password;
    print("Configuracoes iniciais do objeto");
  }

  void autenticator() {
    var user = "renan";
    var password = "123456";

    if (this.user == user && password == password) {
      print("pasosu");
    } else {
      print("nao passou");
    }
  }
}

void main() {
  //02 - Passando  os dados por parametros
  User user = User("renan@gmail.com", "1234567");

  user.autenticator();
  /*
  */
}
