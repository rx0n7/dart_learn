class User {
  String user;
  String password;
  //02 -
  String position;

  User(this.user, this.password);

  //01 - Construtor nomeado chamado "director",
  User.director(this.user, this.password) {
    //03 -
    this.position = "diretor";
    //04
    print("Libera privilegios para o $position");
  }

  void autenticator() {
    var user = "renan";
    var password = "123456";

    if (this.user == user && password == password) {
      print("pasosu");
    } else {
      print("nao passou");
    }
  }
}

void main() {
  //05
  User userDirector = User.director("renan@gmail.com", "1234567");

  userDirector.autenticator();
  /*
    Libera privilegios para o diretor
    nao passou
  */
}
