class User {
  String name;
  String password;

  //01 - Passando dados pelo construtor, que tem o mesmo nome da classe "User"
  User() {
    print("Configuracoes iniciais do objeto");
  }

  void autenticator() {
    var name = "renan";
    var password = "123456";

    if (this.name == name && password == password) {
      print("pasosu");
    } else {
      print("nao passou");
    }
  }
}

void main() {
  User user = User();

  user.name = "renan2";
  user.password = "1234567";

  user.autenticator();
  /* PS. O construtor eh chamdado quando cria o objeto
    Configuracoes iniciais do objeto

    nao passou
  */
}
